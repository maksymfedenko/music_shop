/* eslint-disable import/no-extraneous-dependencies */

import webpack from 'webpack';
import path from 'path';
import HtmlWebpackPlugin from 'html-webpack-plugin';

export const PATHS = {
  src: path.join(__dirname, '../app'),
  theme: path.join(__dirname, '../app/theme'),
  build: path.join(__dirname, '../build'),
};

export default {
  resolve: {
    modules: [PATHS.src, 'node_modules'],
  },
  entry: ['babel-polyfill', PATHS.src + '/index.js'],
  output: {
    path: PATHS.build,
    filename: './js/[name]__[hash].js',
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'CSV Parsing Service',
      chunks: ['main', 'common'],
      template: 'app/index.html',
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'common',
    }),
    new webpack.ProvidePlugin({
      React: 'react',
    }),
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              'es2015',
              'es2016',
              'react',
              'stage-0',
            ],
            plugins: [
              'transform-object-rest-spread',
            ],
            retainLines: true,
          },
        },
      },
      {
        test: /\.(jpg|png|svg|eot|ttf|woff2?)$/,
        loader: 'file-loader',
      },
    ],
  },
};
