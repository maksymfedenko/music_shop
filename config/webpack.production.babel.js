/* eslint-disable import/no-extraneous-dependencies */

import webpack from 'webpack';
import merge from 'webpack-merge';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import commonConfig, { PATHS } from './webpack.common.babel';

export default merge([
  commonConfig,
  {
    module: {
      rules: [
        {
          test: /\.scss$/,
          exclude: PATHS.theme,
          use: ExtractTextPlugin.extract({
            publicPath: '../',
            fallback: 'style-loader',
            use: [
              {
                loader: 'css-loader',
                options: {
                  modules: true,
                  sourceMap: true,
                  localIdentName: '[local]__[hash:base64:5]',
                },
              },
              'sass-loader',
            ],
          }),
        }, {
          test: /\.scss$/,
          include: PATHS.theme,
          use: ExtractTextPlugin.extract({
            publicPath: '../',
            fallback: 'style-loader',
            use: [
              {
                loader: 'css-loader',
                options: {
                  sourceMap: true,
                },
              },
              'sass-loader',
            ],
          }),
        }, {
          test: /\.css$/,
          use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: [
              {
                loader: 'css-loader',
                options: {
                  modules: true,
                  sourceMap: true,
                  localIdentName: '[local]__[hash:base64:5]',
                },
              },
            ],
          }),
        },
      ],
    },
    plugins: [
      new ExtractTextPlugin('./css/[name]__[hash].css'),
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: "\"production\"", // eslint-disable-line quotes
        },
      }),
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          warnings: false,
        },
        uglifyOptions: {
          ecma: 7,
        },
      }),
    ],
  },
]);
