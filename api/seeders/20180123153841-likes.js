const flatten = require('lodash/flatten');
const { Album, Author, Song, User } = require('../models');
const getRandomElemArray = require('../utils/getRandomElemArray');
const getRandomSubArray = require('../utils/getRandomSubArray');

const quantityOfLikes = [1, 2, 3];

const getLikesAttrs = (entities, type, users) => {
  return flatten(users.map((user) => {
    const quantity = getRandomElemArray(quantityOfLikes);
    return getRandomSubArray(entities, quantity).map((entity) => ({
      relatedType: type,
      relatedId: entity.get('id'),
      userId: user.get('id'),
      type: 'like',
    }));
  }));
};

module.exports = {
  up: (queryInterface) => {
    return Promise.all([
      Album.findAll({ attributes: ['id'] }),
      Author.findAll({ attributes: ['id'] }),
      Song.findAll({ attributes: ['id'] }),
      User.findAll({ attributes: ['id'] }),
    ]).then(([albums, authors, songs, users]) => {
      return queryInterface.bulkInsert(
        'Likes',
        [
          ...getLikesAttrs(albums, 'Album', users),
          ...getLikesAttrs(authors, 'Author', users),
          ...getLikesAttrs(songs, 'Song', users),
        ],
      );
    });
  },

  down: (queryInterface) => {
    return queryInterface.bulkDelete('Likes', null, {});
  },
};
