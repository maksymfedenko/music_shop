const PlaylistFactory = require('../factories/playlist');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.bulkInsert('Playlists', PlaylistFactory.buildList(5), {});
  },

  down: (queryInterface) => {
    return queryInterface.bulkDelete('Playlists', { userId: null }, {});
  },
};
