const AuthorFactory = require('../factories/author');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.bulkInsert('Authors', AuthorFactory.buildList(3), {});
  },

  down: (queryInterface) => {
    return queryInterface.bulkDelete('Authors', null, {});
  },
};
