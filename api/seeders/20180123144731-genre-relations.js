const flatten = require('lodash/flatten');
const { Album, Author, Song, User, Genre } = require('../models');
const getRandomElemArray = require('../utils/getRandomElemArray');
const getRandomSubArray = require('../utils/getRandomSubArray');

const quantityOfGenres = [0, 1, 2];

const getGenreRelationsAttrs = (entities, type, genres) => {
  return flatten(entities.map((entity) => {
    const quantity = getRandomElemArray(quantityOfGenres);
    return getRandomSubArray(genres, quantity).map((genre) => ({
      relatedType: type,
      relatedId: entity.get('id'),
      genreId: genre.get('id'),
    }));
  }));
};

module.exports = {
  up: (queryInterface) => {
    return Promise.all([
      Album.findAll({ attributes: ['id'] }),
      Author.findAll({ attributes: ['id'] }),
      Song.findAll({ attributes: ['id'] }),
      User.findAll({ attributes: ['id'] }),
      Genre.findAll({ attributes: ['id'] }),
    ]).then(([albums, authors, songs, users, genres]) => {
      return queryInterface.bulkInsert(
        'GenreRelations',
        [
          ...getGenreRelationsAttrs(authors, 'Author', genres),
          ...getGenreRelationsAttrs(albums, 'Album', genres),
          ...getGenreRelationsAttrs(songs, 'Song', genres),
          ...getGenreRelationsAttrs(users, 'User', genres),
        ],
      );
    });
  },

  down: (queryInterface) => {
    return queryInterface.bulkDelete('GenreRelations', null, {});
  },
};
