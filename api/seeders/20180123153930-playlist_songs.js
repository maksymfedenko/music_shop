const flatten = require('lodash/flatten');
const { Playlist, Song } = require('../models');
const getRandomElemArray = require('../utils/getRandomElemArray');
const getRandomSubArray = require('../utils/getRandomSubArray');

const quantityOfSongs = [1, 2, 4];

const getPlaylistSongsAttrs = (playlists, songs) => {
  return flatten(playlists.map((playlist) => {
    const quantity = getRandomElemArray(quantityOfSongs);
    return getRandomSubArray(songs, quantity).map((song) => ({
      playlistId: playlist.get('id'),
      songId: song.get('id'),
    }));
  }));
};

module.exports = {
  up: (queryInterface) => {
    return Promise.all([
      Playlist.findAll({ attributes: ['id'] }),
      Song.findAll({ attributes: ['id'] }),
    ]).then(([playlists, songs]) => {
      return queryInterface.bulkInsert('PlaylistSongs', getPlaylistSongsAttrs(playlists, songs), {});
    });
  },

  down: (queryInterface) => {
    return queryInterface.bulkDelete('PlaylistSongs', null, {});
  },
};
