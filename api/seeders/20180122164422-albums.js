const AlbumFactory = require('../factories/album');
const { Author } = require('../models');
const getRandomElemArray = require('../utils/getRandomElemArray');

module.exports = {
  up: (queryInterface) => {
    return Author.findAll({ attributes: ['id'] })
      .then((authors) => {
        return queryInterface.bulkInsert(
          'Albums',
          AlbumFactory.buildList(10).map((album) => ({
            ...album,
            authorId: getRandomElemArray(authors).get('id'),
          })),
          {},
        );
      });
  },

  down: (queryInterface) => {
    return queryInterface.bulkDelete('Albums', null, {});
  },
};
