const SongFactory = require('../factories/song');
const { Album } = require('../models');
const getRandomElemArray = require('../utils/getRandomElemArray');

module.exports = {
  up: (queryInterface) => {
    return Album.findAll({ attributes: ['id'] })
      .then((albums) => {
        return queryInterface.bulkInsert(
          'Songs',
          SongFactory.buildList(25).map((song) => ({
            ...song,
            albumId: getRandomElemArray(albums).get('id'),
          })),
          {},
        );
      });
  },

  down: (queryInterface) => {
    return queryInterface.bulkDelete('Songs', null, {});
  },
};
