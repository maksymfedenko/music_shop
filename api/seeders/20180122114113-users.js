const UserFactory = require('../factories/user');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.bulkInsert('Users', UserFactory.buildList(5), {});
  },

  down: (queryInterface) => {
    return queryInterface.bulkDelete('Users', null, {});
  },
};
