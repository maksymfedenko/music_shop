const GenreFactory = require('../factories/genre');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.bulkInsert('Genres', GenreFactory.buildList(5), {});
  },

  down: (queryInterface) => {
    return queryInterface.bulkDelete('Genres', null, {});
  },
};
