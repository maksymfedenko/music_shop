const fs = require('fs');
const path = require('path');
const chai = require('chai'); // eslint-disable-line import/no-extraneous-dependencies
const chaiHttp = require('chai-http'); // eslint-disable-line import/no-extraneous-dependencies
const db = require('../../models');
const app = require('../../index');

const basename = path.basename(__filename);


const should = chai.should(); // eslint-disable-line no-unused-vars
chai.use(chaiHttp);

const request = {
  get: (route) => chai.request(app).get(`/api${route}`),
  post: (route) => chai.request(app).post(`/api${route}`),
  put: (route) => chai.request(app).put(`/api${route}`),
  delete: (route) => chai.request(app).delete(`/api${route}`),
};

fs
  .readdirSync(__dirname)
  .filter((file) => (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js'))
  .forEach((file) => {
    const test = require(`./${file}`); // eslint-disable-line global-require, import/no-dynamic-require
    test(request, db);
  });

