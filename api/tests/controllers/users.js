/* eslint-disable no-undef */
const UserFactory = require('../../factories/user');

module.exports = (request, db) => {
  describe('Users', () => {

    before((done) => {
      db.User.destroy({ where: {} }).then(() => done());
    });

    describe('index', () => {
      const initialUsersCount = 2;

      before((done) => {
        db.User.bulkCreate(UserFactory.buildList(initialUsersCount)).then(() => done());
      });

      it('it should be success', (done) => {
        request.get('/users')
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });

      it('it should GET all users', (done) => {
        request.get('/users')
          .end((err, res) => {
            res.body.should.be.a('object');
            res.body.should.have.property('resources');
            res.body.resources.should.be.a('array');
            res.body.resources.length.should.be.eql(initialUsersCount);
            done();
          });
      });

      it('it should GET users with correct fields', (done) => {
        request.get('/users')
          .end((err, res) => {
            res.body.resources[0].should.have.include.all.keys(
              'id',
              'email',
              'firstName',
              'lastName',
              'createdAt',
              'updatedAt',
            );
            done();
          });
      });
    });

    describe('create', () => {
      const params = { resource: UserFactory.attributes() };

      it('it should be success', (done) => {
        console.log('LOL', params);
        request.post('/users')
          .send(params)
          .end((err, res) => {
            console.log('???', res.body);
            res.should.have.status(200);
            done();
          });
      });

      it('it shoud respond with user data', (done) => {
        request.post('/users')
          .send(params)
          .end((err, res) => {
            res.body.should.be.a('object');
            res.body.should.have.property('resource');
            res.body.resource.should.be.a('object');
            done();
          });
      });

      it('it should respond with user with correct fields', (done) => {
        request.post('/users')
          .send(params)
          .end((err, res) => {
            res.body.resource.should.have.include.all.keys(
              'id',
              'email',
              'firstName',
              'lastName',
              'createdAt',
              'updatedAt',
            );
            done();
          });
      });

      it('it should respond with user with correct data', (done) => {
        request.post('/users')
          .send(params)
          .end((err, res) => {
            res.body.resource.should.include(params.resource);
            done();
          });
      });
    });

    describe('show', () => {
      let userId;

      before((done) => {
        db.User.create(UserFactory.build())
          .then((user) => {
            userId = user.get('id');
            done();
          });
      });

      it('it should be success', (done) => {
        request.get(`/users/${userId}`)
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });

      it('it should GET user', (done) => {
        request.get(`/users/${userId}`)
          .end((err, res) => {
            res.body.should.be.a('object');
            res.body.should.have.property('resource');
            res.body.resource.should.be.a('object');
            done();
          });
      });

      it('it should GET correct user', (done) => {
        request.get(`/users/${userId}`)
          .end((err, res) => {
            res.body.resource.id.should.be.eql(userId);
            done();
          });
      });

      it('it should GET user with correct fields', (done) => {
        request.get(`/users/${userId}`)
          .end((err, res) => {
            res.body.resource.should.have.include.all.keys(
              'id',
              'email',
              'firstName',
              'lastName',
              'createdAt',
              'updatedAt',
            );
            done();
          });
      });
    });

    describe('update', () => {
      const params = { resource: UserFactory.attributes() };
      let userId;

      before((done) => {
        db.User.create(UserFactory.build())
          .then((user) => {
            userId = user.get('id');
            done();
          });
      });

      it('it should be success', (done) => {
        request.put(`/users/${userId}`)
          .send(params)
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });

      it('it should has correct response', (done) => {
        request.put(`/users/${userId}`)
          .send(params)
          .end((err, res) => {
            res.body.should.be.a('object');
            res.body.should.have.property('resource');
            res.body.resource.should.be.a('object');
            done();
          });
      });

      it('it should respond with correct user', (done) => {
        request.put(`/users/${userId}`)
          .send(params)
          .end((err, res) => {
            res.body.resource.id.should.be.eql(userId);
            done();
          });
      });

      it('it should GET users with correct fields', (done) => {
        request.put(`/users/${userId}`)
          .end((err, res) => {
            res.body.resource.should.have.include.all.keys(
              'id',
              'email',
              'firstName',
              'lastName',
              'createdAt',
              'updatedAt',
            );
            done();
          });
      });

      it('it should GET users with correct data', (done) => {
        request.put(`/users/${userId}`)
          .end((err, res) => {
            res.body.resource.should.include(params.resource);
            done();
          });
      });
    });

    describe('delete', () => {
      let userId;

      beforeEach((done) => {
        db.User.create(UserFactory.build())
          .then((user) => {
            userId = user.get('id');
            done();
          });
      });

      it('it should be success', (done) => {
        request.delete(`/users/${userId}`)
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
    });

  });
};
