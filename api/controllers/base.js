class BaseController {
  static getPaginationParams({ query }) {
    const page = parseInt(query.page || 1, 10);
    const perPage = parseInt(query.perPage || 25, 10);

    return {
      offset: (page - 1) * perPage,
      limit: perPage,
    };
  }

  static formatResources(data) {
    return { resources: data };
  }

  static formatResource(data) {
    return { resource: data };
  }

  static formatErrors(errors) {
    return {
      errors: typeof errors === 'string'
        ? [errors]
        : errors,
    };
  }

  static getRouter() {
    throw new Error('Method (getRouter) not implemented', 'api/controllers/base.js');
  }
}

module.exports = BaseController;
