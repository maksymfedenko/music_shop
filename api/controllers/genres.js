
const { Router } = require('express');
const { Genre } = require('../models');
const BaseController = require('./base');

class GenresController extends BaseController {
  static getRouter() {
    const router = Router();

    router.get('/', (req, res) => {
      Genre.findAll(this.getPaginationParams(req))
        .then((genres) => {
          res.json(this.formatResources(genres));
        });
    });

    router.post('/', (req, res) => {
      Genre.create(req.body.resource)
        .then((genre) => {
          res.json(this.formatResource(genre));
        });
    });

    router.route('/:genreId')
      .all((req, res, next) => {
        Genre.findById(req.params.genreId)
          .then((genre) => {
            res.locals.genre = genre;
            next();
          })
          .catch(() => {
            res.status(500);
            res.json(this.formatErrors('Record not found'));
          });
      })
      .get((req, res) => {
        res.json(this.formatResource(res.locals.genre));
      })
      .put((req, res) => {
        res.locals.genre.update({
          ...req.body.resource,
          updatedAt: new Date(),
        });
        res.json(this.formatResource(res.locals.genre));
      })
      .delete((req, res) => {
        res.locals.genre.destroy();
        res.sendStatus(200);
      });

    return router;
  }
}

module.exports = GenresController;
