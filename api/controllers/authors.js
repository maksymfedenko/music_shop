
const { Router } = require('express');
const { Author } = require('../models');
const BaseController = require('./base');

class AuthorsController extends BaseController {
  static getRouter() {
    const router = Router();

    router.get('/', (req, res) => {
      Author.findAll(this.getPaginationParams(req))
        .then((authors) => {
          res.json(this.formatResources(authors));
        });
    });

    router.post('/', (req, res) => {
      Author.create(req.body.resource)
        .then((author) => {
          res.json(this.formatResource(author));
        });
    });

    router.route('/:authorId')
      .all((req, res, next) => {
        Author.findById(req.params.authorId)
          .then((author) => {
            res.locals.author = author;
            next();
          })
          .catch(() => {
            res.status(500);
            res.json(this.formatErrors('Record not found'));
          });
      })
      .get((req, res) => {
        res.json(this.formatResource(res.locals.author));
      })
      .put((req, res) => {
        res.locals.author.update({
          ...req.body.resource,
          updatedAt: new Date(),
        });
        res.json(this.formatResource(res.locals.author));
      })
      .delete((req, res) => {
        res.locals.author.destroy();
        res.sendStatus(200);
      });

    return router;
  }
}

module.exports = AuthorsController;
