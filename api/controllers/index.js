const { Router } = require('express');
const UsersController = require('./users');
const GenresController = require('./genres');
const AuthorsController = require('./authors');
const AlbumsController = require('./albums');
const SongsController = require('./songs');

const attachControllers = (app) => {
  const apiRouter = Router();

  apiRouter.use('/users', UsersController.getRouter());
  apiRouter.use('/genres', GenresController.getRouter());
  apiRouter.use('/authors', AuthorsController.getRouter());
  apiRouter.use('/albums', AlbumsController.getRouter());
  apiRouter.use('/songs', SongsController.getRouter());

  app.use('/api', apiRouter);
};

module.exports = attachControllers;
