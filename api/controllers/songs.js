
const { Router } = require('express');
const { Song } = require('../models');
const BaseController = require('./base');

class SongsController extends BaseController {
  static getRouter() {
    const router = Router();

    router.get('/', (req, res) => {
      Song.findAll(this.getPaginationParams(req))
        .then((songs) => {
          res.json(this.formatResources(songs));
        });
    });

    router.post('/', (req, res) => {
      Song.create(req.body.resource)
        .then((song) => {
          res.json(this.formatResource(song));
        });
    });

    router.route('/:songId')
      .all((req, res, next) => {
        Song.findById(req.params.songId)
          .then((song) => {
            res.locals.song = song;
            next();
          })
          .catch(() => {
            res.status(500);
            res.json(this.formatErrors('Record not found'));
          });
      })
      .get((req, res) => {
        res.json(this.formatResource(res.locals.song));
      })
      .put((req, res) => {
        res.locals.song.update({
          ...req.body.resource,
          updatedAt: new Date(),
        });
        res.json(this.formatResource(res.locals.song));
      })
      .delete((req, res) => {
        res.locals.song.destroy();
        res.sendStatus(200);
      });

    return router;
  }
}

module.exports = SongsController;
