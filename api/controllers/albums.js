
const { Router } = require('express');
const { Album } = require('../models');
const BaseController = require('./base');

class AlbumsController extends BaseController {
  static getRouter() {
    const router = Router();

    router.get('/', (req, res) => {
      Album.findAll(this.getPaginationParams(req))
        .then((albums) => {
          res.json(this.formatResources(albums));
        });
    });

    router.post('/', (req, res) => {
      Album.create(req.body.resource)
        .then((album) => {
          res.json(this.formatResource(album));
        });
    });

    router.route('/:albumId')
      .all((req, res, next) => {
        Album.findById(req.params.albumId)
          .then((album) => {
            res.locals.album = album;
            next();
          })
          .catch(() => {
            res.status(500);
            res.json(this.formatErrors('Record not found'));
          });
      })
      .get((req, res) => {
        res.json(this.formatResource(res.locals.album));
      })
      .put((req, res) => {
        res.locals.album.update({
          ...req.body.resource,
          updatedAt: new Date(),
        });
        res.json(this.formatResource(res.locals.album));
      })
      .delete((req, res) => {
        res.locals.album.destroy();
        res.sendStatus(200);
      });

    return router;
  }
}

module.exports = AlbumsController;
