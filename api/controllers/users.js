
const { Router } = require('express');
const { User } = require('../models');
const BaseController = require('./base');

class UsersController extends BaseController {
  static getRouter() {
    const router = Router();

    router.get('/', (req, res) => {
      User.findAll(this.getPaginationParams(req))
        .then((users) => {
          res.json(this.formatResources(users));
        });
    });

    router.post('/', (req, res) => {
      console.log('XXXX', req.body);
      User.create(req.body.resource)
        .then((user) => {
          console.log('SSSSSSSSSS', user);
          res.json(this.formatResource(user));
        });
    });

    router.route('/:userId')
      .all((req, res, next) => {
        User.findById(req.params.userId)
          .then((user) => {
            res.locals.user = user;
            next();
          })
          .catch(() => {
            res.status(500);
            res.json(this.formatErrors('Record not found'));
          });
      })
      .get((req, res) => {
        res.json(this.formatResource(res.locals.user));
      })
      .put((req, res) => {
        res.locals.user.update({
          ...req.body.resource,
          updatedAt: new Date(),
        });
        res.json(this.formatResource(res.locals.user));
      })
      .delete((req, res) => {
        res.locals.user.destroy();
        res.sendStatus(200);
      });

    return router;
  }
}

module.exports = UsersController;
