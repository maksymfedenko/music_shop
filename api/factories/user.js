const { Factory } = require('rosie');
const faker = require('faker');
const crypto = require('crypto');

const salt = crypto.randomBytes(16).toString('hex').slice(0, 32);
const defaultPassword = '123123123';

module.exports = new Factory()
  .sequence('id')
  .attrs({
    firstName: () => faker.name.firstName(),
    lastName: () => 'Ololovich',
    email: () => faker.internet.email(),
    salt,
    encryptedPassword: () => crypto.createHmac('sha512', salt).update(defaultPassword).digest('hex'),
  });
