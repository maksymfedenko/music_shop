const { Factory } = require('rosie');
const faker = require('faker');


module.exports = new Factory()
  .sequence('id')
  .attrs({
    name: () => `${faker.name.firstName()} ${faker.name.lastName()}`,
  });
