const { Factory } = require('rosie');
const getRandomElemArray = require('../utils/getRandomElemArray');

let availableGenres = [
  'Alternative',
  'Blues',
  'Classical',
  'Country',
  'Dance',
  'Electronic',
  'Folk',
  'Hip Hop',
  'Rap',
  'Indie',
  'Inspirational',
  'J-Pop',
  'Jazz',
  'Latin',
  'Opera',
  'Pop',
  'R&B',
  'Reggae',
  'Rock',
];

module.exports = new Factory()
  .sequence('id')
  .attrs({
    name: () => {
      const genre = getRandomElemArray(availableGenres);
      availableGenres = availableGenres.filter((genreName) => genreName !== genre);
      return genre;
    },
  });
