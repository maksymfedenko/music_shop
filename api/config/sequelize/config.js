module.exports = {
  development: {
    username: 'username',
    // password: '123',
    database: 'ms_node_development',
    host: '127.0.0.1',
    dialect: 'postgres',
  },
  test: {
    username: 'username',
    // password: '123',
    database: 'ms_node_test',
    host: '127.0.0.1',
    dialect: 'postgres',
  },
  production: {
    username: process.env.PROD_DB_USERNAME,
    password: process.env.PROD_DB_PASSWORD,
    database: process.env.PROD_DB_NAME,
    host: process.env.PROD_DB_HOSTNAME,
    dialect: 'postgres',
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000,
    },
  },
};
