const Sequelize = require('sequelize');
const allConfigs = require('./config');

const env = process.NODE_ENV || 'development';
const config = allConfigs[env];

const sequelize = new Sequelize(config.database, config.username, config.password, config);

sequelize
  .authenticate()
  .then(() => {
    // eslint-disable-next-line no-console
    console.log('Connection has been established successfully.');
  })
  .catch((err) => {
    // eslint-disable-next-line no-console
    console.error('Unable to connect to the database:', err);
  });

module.exports = sequelize;
module.exports.Sequelize = Sequelize;
