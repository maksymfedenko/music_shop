const express = require('express');
const morgan = require('morgan');
const attachControllers = require('./controllers');

const app = express();

if (process.env.NODE_ENV !== 'test') app.use(morgan('dev'));
attachControllers(app);

app.listen(5000);

module.exports = app;
