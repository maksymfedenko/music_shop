module.exports = (array, amount) => {
  const randomIndex = Math.floor(Math.random() * array.length);
  if (!amount) return [];
  // code below not really return array with random elements
  if (amount + randomIndex < array.length) return array.slice(randomIndex, randomIndex + amount - 1);
  return array.slice(randomIndex - array.length, randomIndex + amount - array.length - 1);
};
