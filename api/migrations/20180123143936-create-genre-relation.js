module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('GenreRelations', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      genreId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Genres',
          key: 'id',
          onDelete: 'cascade',
        },
      },
      relatedId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      relatedType: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('now'),
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('now'),
      },
    });
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('GenreRelations');
  },
};
