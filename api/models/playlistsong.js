module.exports = (sequelize, DataTypes) => {
  const PlaylistSong = sequelize.define('PlaylistSong', {
    playlistId: DataTypes.INTEGER,
    songId: DataTypes.INTEGER,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  }, {
    classMethods: {
      associate: (models) => {
        PlaylistSong.belongsTo(models.Playlist);
        PlaylistSong.belongsTo(models.Song);
      },
    },
  });
  return PlaylistSong;
};
