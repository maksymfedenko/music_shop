module.exports = (sequelize, DataTypes) => {
  const Album = sequelize.define('Album', {
    name: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  }, {
    classMethods: {
      associate: (models) => {
        Album.belongsTo(models.Author);
        Album.hasMany(models.Song, { onDelete: 'cascade' });

        Album.belongsToMany(models.Genre, {
          through: {
            foreignKey: 'relatedId',
            model: models.GenreRelation,
            onDelete: 'cascade',
            unique: false,
            scope: {
              relatedType: Album.name,
            },
          },
        });

        Album.belongsToMany(models.User, {
          through: {
            foreignKey: 'relatedId',
            model: models.Like,
            unique: false,
            onDelete: 'cascade',
            scope: {
              relatedType: Album.name,
            },
          },
        });
      },
    },
    indexes: [
      {
        fields: ['name'],
      },
    ],
  });
  return Album;
};
