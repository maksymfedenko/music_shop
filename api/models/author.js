module.exports = (sequelize, DataTypes) => {
  const Author = sequelize.define('Author', {
    name: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  }, {
    classMethods: {
      associate: (models) => {
        Author.hasMany(models.Album, { onDelete: 'cascade' });

        Author.belongsToMany(models.Genre, {
          through: {
            foreignKey: 'relatedId',
            model: models.GenreRelation,
            unique: false,
            onDelete: 'cascade',
            scope: {
              relatedType: Author.name,
            },
          },
        });

        Author.belongsToMany(models.User, {
          through: {
            model: models.Like,
            foreignKey: 'relatedId',
            onDelete: 'cascade',
            unique: false,
            scope: {
              relatedType: Author.name,
            },
          },
        });
      },
    },
    indexes: [
      {
        fields: ['name'],
      },
    ],
  });
  return Author;
};
