module.exports = (sequelize, DataTypes) => {
  const GenreRelation = sequelize.define('GenreRelation', {
    genreId: DataTypes.INTEGER,
    relatedId: DataTypes.INTEGER,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  }, {});
  return GenreRelation;
};
