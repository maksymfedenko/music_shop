module.exports = (sequelize, DataTypes) => {
  const Song = sequelize.define('Song', {
    name: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  }, {
    classMethods: {
      associate: (models) => {
        Song.belongsTo(models.Album);
        Song.belongsTo(models.User);
        Song.hasMany(models.PlaylistSong, { onDelete: 'cascade' });

        Song.belongsToMany(models.Genre, {
          through: {
            model: models.GenreRelation,
            foreignKey: 'relatedId',
            unique: false,
            scope: {
              relatedType: Song.name,
            },
          },
        });

        Song.belongsToMany(models.User, {
          through: {
            model: models.Like,
            foreignKey: 'relatedId',
            unique: false,
            scope: {
              relatedType: Song.name,
            },
          },
          as: 'UsersGivedFeedback',
        });

        Song.belongsToMany(models.User, {
          through: {
            model: models.Like,
            foreignKey: 'relatedId',
            unique: false,
            scope: {
              relatedType: Song.name,
              type: 'like',
            },
          },
          as: 'UsersLiked',
        });

        Song.belongsToMany(models.User, {
          through: {
            model: models.Like,
            foreignKey: 'relatedId',
            unique: false,
            scope: {
              relatedType: Song.name,
              type: 'unlike',
            },
          },
          as: 'UsersHated',
        });
      },
    },
    indexes: [
      {
        fields: ['name'],
      },
    ],
  });
  return Song;
};
