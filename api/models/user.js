const crypto = require('crypto');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    firstName: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        isAlpha: true,
        notEmpty: true,
      },
    },
    lastName: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        isAlpha: true,
        notEmpty: true,
      },
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  }, {
    classMethods: {
      associate: (models) => {
        User.hasMany(models.Playlist, { onDelete: 'cascade' });
        User.hasMany(models.Song, { onDelete: 'cascade' });
        User.hasMany(models.Like, { onDelete: 'cascade' });
        User.hasMany(models.GenreRelation, {
          foreignKey: 'relatedId',
          unique: false,
          onDelete: 'cascade',
          scope: {
            relatedType: User.name,
          },
        });

        User.belongsToMany(models.Genre, {
          through: {
            model: models.GenreRelation,
            foreignKey: 'relatedId',
            unique: false,
            scope: {
              relatedType: User.name,
            },
          },
        });

        User.belongsToMany(models.Song, {
          through: {
            model: models.Like,
            unique: false,
            scope: {
              relatedType: models.Song.name,
            },
          },
          as: 'LikedSongs',
        });

        User.belongsToMany(models.Album, {
          through: {
            model: models.Like,
            unique: false,
            scope: {
              relatedType: models.Album.name,
            },
          },
          as: 'LikedAlbums',
        });

        User.belongsToMany(models.Author, {
          through: {
            model: models.Like,
            unique: false,
            scope: {
              relatedType: models.Author.name,
            },
          },
          as: 'LikedAuthors',
        });
      },
    },
    getterMethods: {
      fullName() {
        return this.firstName + ' ' + this.lastName;
      },
    },
    setterMethods: {
      password(value) {
        let { salt } = this;
        if (!salt) {
          salt = crypto.randomBytes(16).toString('hex').slice(0, 32);
        }
        this.set({
          encryptedPassword: crypto.createHmac('sha512', salt).update(value).digest('hex'),
          salt,
        });
      },
    },
    indexes: [
      {
        fields: ['firstName', 'lastName', 'email'],
      },
    ],
  });
  return User;
};
