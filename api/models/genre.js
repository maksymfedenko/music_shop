module.exports = (sequelize, DataTypes) => {
  const Genre = sequelize.define('Genre', {
    name: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  }, {
    classMethods: {
      associate: (models) => {
        Genre.belongsToMany(models.Album, {
          through: {
            model: models.GenreRelation,
            unique: false,
            onDelete: 'cascade',
            scope: {
              relatedType: models.Album.name,
            },
          },
        });

        Genre.belongsToMany(models.Song, {
          through: {
            model: models.GenreRelation,
            unique: false,
            onDelete: 'cascade',
            scope: {
              relatedType: models.Song.name,
            },
          },
        });

        Genre.belongsToMany(models.Author, {
          through: {
            model: models.GenreRelation,
            unique: false,
            onDelete: 'cascade',
            scope: {
              relatedType: models.Author.name,
            },
          },
        });

        Genre.belongsToMany(models.User, {
          through: {
            model: models.GenreRelation,
            unique: false,
            onDelete: 'cascade',
            scope: {
              relatedType: models.User.name,
            },
          },
        });
      },
    },
  });
  return Genre;
};
