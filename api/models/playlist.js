module.exports = (sequelize, DataTypes) => {
  const Playlist = sequelize.define('Playlist', {
    userId: DataTypes.INTEGER,
    name: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  }, {
    classMethods: {
      associate: (models) => {
        Playlist.belongsTo(models.User);
        Playlist.hasMany(models.PlaylistSong, { onDelete: 'cascade' });
      },
    },
  });
  return Playlist;
};
