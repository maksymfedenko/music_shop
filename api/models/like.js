module.exports = (sequelize, DataTypes) => {
  const Like = sequelize.define('Like', {
    type: {
      type: DataTypes.ENUM,
      values: ['like', 'unlike'],
    },
    relatedType: DataTypes.STRING,
    relatedId: DataTypes.INTEGER,
    userId: DataTypes.INTEGER,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  }, {
    classMethods: {
      associate: (models) => {
        Like.belongsTo(models.User);
      },
    },
  });
  return Like;
};
