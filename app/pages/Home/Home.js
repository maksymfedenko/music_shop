import React from 'react';
import { Spinner } from 'components';

const Home = () => (
  <section>
    Home
    <Spinner />
    <Spinner type="music" />
    <Spinner type="domino" />
    <br />
    <Spinner size="20px" />
    <Spinner type="music" size="20px" />
    <Spinner type="domino" size="20px" />
    <br />
    <Spinner size="100px" />
    <Spinner type="music" size="100px" />
    <Spinner type="domino" size="100px" />
    <br />
  </section>
);

export default Home;
