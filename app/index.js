import React from 'react';
import { Provider } from 'react-redux';
import LayoutManager from 'layouts/LayoutManager/LayoutManager';
import { render } from 'react-dom';
import { ConnectedRouter } from 'react-router-redux';
import store, { history } from 'config/store';
import 'normalize.css';
import 'theme/global.scss';

render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <LayoutManager />
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root'),
);
