import React from 'react';
import { Route } from 'react-router-dom';
import { Home } from 'pages';
import { Header, Footer } from 'components';
import styles from './DefaultLayout.scss';

export const DefaultLayout = () => (
  <div className={styles.DefaultLayout}>
    <Header />
    <main className={styles.mainSection}>
      DefaultLayout
      <Route exact path="/" component={Home} />
    </main>
    <Footer />
  </div>
);

export default DefaultLayout;
