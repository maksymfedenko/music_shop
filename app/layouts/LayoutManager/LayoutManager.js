import React from 'react';
import { Route } from 'react-router-dom';
import DefaultLayout from 'layouts/DefaultLayout/DefaultLayout';
import MinimalLayout from 'layouts/MinimalLayout/MinimalLayout';
import styles from './LayoutManager.scss';

export const LayoutManager = () => (
  <div className={styles.LayoutManager}>
    <Route exact path="/" component={DefaultLayout} />
    <Route exact path="/about-us" component={MinimalLayout} />
    <div id="portalContainer" />
  </div>
);

export default LayoutManager;
