import React from 'react';
import T from 'prop-types';
import cn from 'classnames';
import styles from './Spinner.scss';

const MusicSpinner = ({ show, size }) => (
  <div className={cn(styles.Spinner, styles.music, { show })} style={{ height: size, width: size }}>
    <div className={cn(styles.line, styles.line1)} />
    <div className={cn(styles.line, styles.line2)} />
    <div className={cn(styles.line, styles.line3)} />
  </div>
);

MusicSpinner.propTypes = {
  show: T.bool,
  size: T.string,
};

MusicSpinner.defaultProps = {
  show: true,
  size: '60px',
};

export default MusicSpinner;
