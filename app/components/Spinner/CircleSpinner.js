import React from 'react';
import T from 'prop-types';
import cn from 'classnames';
import styles from './Spinner.scss';

const CircleSpinner = ({ show, size }) => (
  <div className={cn(styles.Spinner, styles.circle, { show })} style={{ height: size, width: size }}>
    <div className={styles.outer} />
    <div className={styles.inner} />
  </div>
);

CircleSpinner.propTypes = {
  show: T.bool,
  size: T.string,
};

CircleSpinner.defaultProps = {
  show: true,
  size: '60px',
};

export default CircleSpinner;
