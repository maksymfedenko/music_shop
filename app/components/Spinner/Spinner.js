import React from 'react';
import T from 'prop-types';
import CircleSpinner from './CircleSpinner';
import MusicSpinner from './MusicSpinner';
import DominoSpinner from './DominoSpinner';

const spinners = {
  default: CircleSpinner,
  circle: CircleSpinner,
  music: MusicSpinner,
  domino: DominoSpinner,
};

const Spinner = ({ type, ...otherProps }) => {
  const SelectedSpinner = spinners[type];

  return (
    <SelectedSpinner {...otherProps} />
  );
};

Spinner.propTypes = {
  type: T.oneOf(['default', 'music', 'domino', 'circle']),
};

Spinner.defaultProps = {
  type: 'default',
};

export default Spinner;
