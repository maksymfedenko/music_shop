import React from 'react';
import T from 'prop-types';
import cn from 'classnames';
import styles from './Spinner.scss';

const DominoSpinner = ({ show, size }) => (
  <div className={cn(styles.Spinner, styles.domino, { show })} style={{ height: size, width: size }}>
    <div className={cn(styles.singleDomino, styles.domino1)} />
    <div className={cn(styles.singleDomino, styles.domino2)} />
    <div className={cn(styles.singleDomino, styles.domino3)} />
    <div className={cn(styles.singleDomino, styles.domino4)} />
    <div className={cn(styles.singleDomino, styles.domino5)} />
    <div className={cn(styles.singleDomino, styles.domino6)} />
    <div className={cn(styles.singleDomino, styles.domino7)} />
    <div className={cn(styles.singleDomino, styles.domino8)} />
  </div>
);

DominoSpinner.propTypes = {
  show: T.bool,
  size: T.string,
};

DominoSpinner.defaultProps = {
  show: true,
  size: '60px',
};

export default DominoSpinner;
