# Music Shop (Late 2017)

### Project has only architecture, and a little bit of backend. Front-end not started.

* My try to maintain back-end and front-end parts in one repository.
* Similar to Ruby on Rails back-end architecture.


## Front-end

### Start

1) npm start

### Build

2) npm run build

## Back-end

### Initialization

1) Install and run PostgreSQL
2) Change username in ./api/config/sequelize/config.js to existing in your postgresql username
3) npm run sq db:create
4) npm run sq db:migrate (cancel - npm run sq db:migrate:undo:all)
5) Generate fake data - npm run sq db:seed:all (cancel - npm run sq db:seed:undo:all)

### Start

1) npm run start:back

### Run tests

1) npm run test:back (not all passed)
