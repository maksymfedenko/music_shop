module.exports = {
  extends: 'airbnb',
  parser: 'babel-eslint',
  env: {
    browser: true,
    commonjs: true,
    es6: true,
    jest: true,
    node: true,
  },
  settings: {
    'import/resolver': {
      node: {
        paths: ['app'],
      },
    },
  },
  rules: {
    // keep alphabet order (asc)
    'arrow-body-style': 'off',
    'arrow-parens': ['error', 'always'],
    'function-paren-newline': 'off',
    'jsx-a11y/anchor-is-valid': 'off',
    'jsx-a11y/label-has-for': 'off',
    'jsx-a11y/no-autofocus': 'off',
    'import/no-named-as-default': 'off',
    'max-len': ['error', {
      code: 120,
      tabWidth: 2,
      ignoreComments: true,
      ignoreTrailingComments: true,
      ignoreUrls: true,
    }],
    'no-mixed-operators': 'off',
    'no-shadow': 'off',
    'no-underscore-dangle': 'off',
    'no-use-before-define': 'off',
    'no-restricted-globals': 'off',
    'object-curly-newline': 'off',
    'padded-blocks': 'off',
    'prefer-template': 'off',
    'react/jsx-filename-extension': 'off',
    'react/react-in-jsx-scope': 'off',
  }
};
